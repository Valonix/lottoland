import * as React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/App.css';
import Layout from './components/Layout';

class App extends React.Component {
	render(): JSX.Element {
		return (
			<div>
				<Layout/>
			</div>
		);
	}
}

export default App;
