'use strict';
const uri: string = 'https://www.lottoland.com/api/drawings/euroJackpot';
const proxyUrl: string = 'https://cors-anywhere.herokuapp.com/';

/**
 * Get data from API
 * @returns {Promise<any>}
 */
export function getResultFromApi(): Promise<any> {
	return fetch(proxyUrl + uri)
			.then(response => response.json())
			.then(contents => contents)
			.catch(() => console.log('Can’t access ' + uri + ' response. Blocked by browser?'));
}
