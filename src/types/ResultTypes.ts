export interface ResultI {
	result?: {
		last?: {
			nr: number;
			currency: string;
			date: DateI;
			closingDate: string;
			lateClosingDate: string;
			drawingDat: string;
			numbers: number[],
			euroNumbers: number[],
			jackpot: string;
			marketingJackpot: string;
			specialMarketingJackpot: string;
			climbedSince: number;
			Winners: number;
			odds: {
				[key: string]: RankI
			};
		};
		next?: {
			nr: number;
			currency: string;
			date: DateI;
			closingDate: string;
			lateClosingDate: string;
			drawingDate: string;
			jackpot: string;
			marketingJackpot: string;
			specialMarketingJackpot: string;
			climbedSince: number;
		};
	};
	windowHeight: number;
	windowWidth: number;
	ready: boolean;
	isMobile: boolean;
}

export interface RankI {
	winners: number;
	specialPrize: number;
	prize: number;
}

export interface DateI {
	full: string;
	day: number;
	month: number;
	year: number;
	hour: number;
	minute: number;
	dayOfWeek: string;
}