import * as React from 'react';

export interface SplitInterface {
	left: JSX.Element;
	right: JSX.Element;
}

class Split extends React.Component <SplitInterface> {
	constructor(props: SplitInterface) {
		super(props);
	}

	render(): JSX.Element {
		const {left, right} = this.props;
		return (
				<main role="main" className="container content">
					<div className="row">
						{left}
						{right}
					</div>
				</main>
		);
	}
}

export default Split;
