import * as React from 'react';

export interface NumbersI {
	numbers: number[];
	euroNumbers: number[];
}

class Numbers extends React.Component <NumbersI, any> {
	constructor(props: NumbersI) {
		super(props);
	}

	render(): JSX.Element {
		const {numbers, euroNumbers} = this.props;
		const items = numbers.map((item, index): JSX.Element =>
				<li className="l-lottery-number" key={index}>{item}</li>);

		const euro = euroNumbers.map((item, index): JSX.Element =>
				<li className="l-lottery-number extra" key={index}>{item}</li>);
		return (
				<div className={'nums'}>
					<div className="l-results-numbers">
						<ul className="l-lottery-numbers-container euroJackpot t-results-balls">
							{items}
							{euro}
						</ul>
					</div>
				</div>
		);
	}
}

export default Numbers;
