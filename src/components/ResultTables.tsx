import * as React from 'react';
import TableItem from './TableItem';

class ResultTables extends React.Component <any, any> {
	constructor(props: any) {
		super(props);
	}

	render(): JSX.Element {
		const { odds, isMobile } = this.props;
		const items = Object.entries(odds).map((item: any, index: any): JSX.Element =>
				<TableItem isMobile={isMobile} key={index} item={item[1]} index={index}/>);

		return (
				<div className="l-results-body-tables t-results-body-tables">
					<table className={`table ${isMobile ? 'styled fixed' : 'l-resultsTable js-results-table'}`}>
						{!isMobile &&
							<thead className="l-resultsTable-head ">
								<tr>
									<th>Tier</th>
									<th>Match</th>
									<th>Winners</th>
									<th>Amount</th>
								</tr>
							</thead>
						}
						<tbody>
							{items}
						</tbody>
					</table>
				</div>
		);
	}
}

export default ResultTables;
