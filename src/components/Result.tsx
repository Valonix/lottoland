import * as React from 'react';
import '../css/lot.css';
import Numbers from './Numbers';
import ResultTables from './ResultTables';

class Result extends React.Component <any, any> {
	constructor(props: any) {
		super(props);
	}

	render(): JSX.Element {
		const {result: {last}, isMobile} = this.props;
		return (
				<div className="col-md-8 blog-main">
					<div>
						<h3 className="pb-3 mb-4 main-header border-bottom">
							EUROJACKPOT RESULTS & WINNING NUMBERS
						</h3>
						<h4>{last.date.full}</h4>
						<Numbers
								euroNumbers={last.euroNumbers}
								numbers={last.numbers}
						/>
						<ResultTables isMobile={isMobile} odds={last.odds}/>
					</div>
				</div>
		);
	}
}

export default Result;
