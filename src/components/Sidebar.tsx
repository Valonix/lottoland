import * as React from 'react';

class Sidebar extends React.Component <any, any> {
	constructor(props: any) {
		super(props);
	}

	render(): JSX.Element {
		const {date} = this.props;
		return (
				<aside className="col-md-4 blog-sidebar">
					<div className="p-3">
						<section className="l-seo-pod">
							<h5 className="l-seo-pod-header">EuroJackpot numbers for {date.day}.{date.month}.{date.year}</h5>
							<p className="l-seo-pod-text"/>
							<p>
								The balls used for the draw are made of a synthetic polymer, softer than ping-pong balls. The results
								are
								broadcast after the draw, with the draw-machines independently checked by the VTT Technical Research
								Center of Finland.
							</p>
							<p>
								Lottoland published the draw results immediately after the draw on 30.03.2018. You can easily check your
								tickets here at Lottoland, or purchase your ticket for the next draw.
							</p>
						</section>
					</div>
				</aside>
		);
	}
}

export default Sidebar;
