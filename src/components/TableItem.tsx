import * as React from 'react';
import * as currency from 'currency.js';

// TODO i am not sure about this logic. So i created this :|
const mathes: string[] = [
	`<div class="td-div">5 Numbers +</div> 2 Euronumbers`,
	`<div class="td-div">5 Numbers +</div> 1 Euronumber`,
	`<div class="td-div">5 Numbers +</div> 0 Euronumbers`,
	`<div class="td-div">4 Numbers +</div> 2 Euronumbers`,
	`<div class="td-div">4 Numbers +</div> 1 Euronumber`,
	`<div class="td-div">4 Numbers +</div> 0 Euronumber`,
	`<div class="td-div">3 Numbers +</div> 2 Euronumbers`,
	`<div class="td-div">2 Numbers +</div> 2 Euronumbers`,
	`<div class="td-div">3 Numbers +</div> 1 Euronumber`,
	`<div class="td-div">3 Numbers +</div> 0 Euronumbers`,
	`<div class="td-div">1 Number + </div>2 Euronumbers`,
	`<div class=td-div>2 Numbers +</div> 1 Euronumber`
];

class TableItem extends React.Component <any, any> {
	constructor(props: any) {
		super(props);
	}

	render(): JSX.Element {
		const { item, index, isMobile } = this.props;
		if (item.prize <= 0) {
			return null;
		}
		const prize = currency(item.prize / 100, {
			formatWithSymbol: true,
			symbol: '€',
			decimal: ',',
			separator: '.'
		}).format();

		const match = mathes[index - 1];
		const win = item.winners.toLocaleString('de-DE', { style: 'decimal'});
		if (isMobile) {
			return (
					<tr>
						<td className="division">
							<span className="tierSpan basic entry">Tier {toRoman(index)}</span>
							<span dangerouslySetInnerHTML={{__html: match}} className="rankSpan entry wrap"/>
						</td>
						<td className="number">
							<span className="tierSpan basic entry  minor">{win}x</span>
							<span className="t-amount rankSpan entry major money">
								<span title={prize}>{prize}</span>
							</span>
						</td>
					</tr>
			);
		}
		return (
				<tr className="td-item">
					<td className="l-resultsTable-data l-resultsTable-data-small-cell  t-tier">
						{toRoman(index)}
					</td>
					<td
							className="l-resultsTable-data l-resultsTable-data-big-cell t-match"
							dangerouslySetInnerHTML={{__html: match}}
					/>
					<td className="l-resultsTable-data l-resultsTable-data-big-cell ">{win}x</td>
					<td className="l-resultsTable-data l-resultsTable-data-no-break l-resultsTable-data-big-cell  t-amount">
						<span><span className="" title="€10,000,000.00">{prize}</span></span>
					</td>
				</tr>
		);
	}
}

export default TableItem;

const fontAr = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000, 4000, 5000, 9000, 10000];
const fontRom = ['I', 'IV', 'V', 'IX', 'X', 'XL', 'L', 'XC', 'C', 'CD', 'D', 'CM', 'M',
	'M&#8577;', '&#8577;', '&#8577;&#8578;', '&#8578;'];

function toRoman(text: any): string {
	if (!text) {
		return '';
	}
	let result = '';
	let n = fontAr.length - 1;
	while (text > 0) {
		if (text >= fontAr[n]) {
			result += fontRom[n];
			text -= fontAr[n];
		} else {
			n--;
		}
	}
	return result;
}
