import * as React from 'react';
import Header from './Header';
import Sidebar from './Sidebar';
import Split from './Split';
import Result from './Result';
import { getResultFromApi } from '../Api';
import { ResultI } from '../types/ResultTypes';

class Layout extends React.Component <any, ResultI> {
	constructor(props: any) {
		super(props);
		this.state = {
			result: {},
			ready: false,
			windowHeight: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
			windowWidth: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
			isMobile: window.innerWidth <= 650,
		};
		this.handleResize = this.handleResize.bind(this);
	}

	componentDidMount() {
		window.addEventListener('resize', this.handleResize);
		getResultFromApi()
				.then((result => {
					this.setState({result, ready: true});
				}));
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.handleResize);
	}

	handleResize(e: any): void {
		const isMobile = window.innerWidth <= 650;
		this.setState({
			windowHeight: window.innerHeight,
			windowWidth: window.innerWidth,
			isMobile,
		});
	}

	render(): JSX.Element {
		const {isMobile, result, ready} = this.state;
		return (
				<div className="layout">
					<Header/>
					{ready ?
							<Split
									left={<Result result={result} isMobile={isMobile}/>}
									right={<Sidebar date={result.last.date}/>}
							/> :
							<div className="container">Loading...</div>
					}
				</div>
		);
	}
}

export default Layout;
