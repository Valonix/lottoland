import * as React from 'react';

class Header extends React.PureComponent {
	render(): JSX.Element {
		return (
				<div className="container">
					<header className="blog-header py-3">
						<div className="row flex-nowrap justify-content-between align-items-center">
							<div className="col text-center">
								<div className="blog-header-logo text-dark">
									<img
											src="https://www.lottoland.com/skins/lottoland/images/logo/ll-logo-green-14c4a0ddaf8c5179.svg"
											alt="Lottoland"
									/>
								</div>
							</div>
						</div>
					</header>
					<div className="nav-scroller py-1 mb-2">
						<nav className="nav d-flex justify-content-between">
							<a className="p-2 text-muted" href="#">World</a>
							<a className="p-2 text-muted" href="#">U.S.</a>
							<a className="p-2 text-muted" href="#">Technology</a>
							<a className="p-2 text-muted" href="#">Design</a>
							<a className="p-2 text-muted" href="#">Culture</a>
							<a className="p-2 text-muted" href="#">Business</a>
							<a className="p-2 text-muted" href="#">Politics</a>
							<a className="p-2 text-muted" href="#">Opinion</a>
							<a className="p-2 text-muted" href="#">Science</a>
							<a className="p-2 text-muted" href="#">Health</a>
							<a className="p-2 text-muted" href="#">Style</a>
							<a className="p-2 text-muted" href="#">Travel</a>
						</nav>
					</div>
				</div>
		);
	}
}

export default Header;
